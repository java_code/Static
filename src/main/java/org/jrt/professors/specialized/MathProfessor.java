package org.jrt.professors.specialized;

import org.jrt.professors.Professor;

/**
 * Class representing math professors
 * 
 * @author jrtobac
 *
 */
public class MathProfessor extends Professor{
	
	private String specialization;
	public static int counter = 0;
	
	/**
	 * static block that gets called at first class reference
	 */
	static {
		System.out.println("In static block of MathProfessor");
	}
	
	/**
	 * Constructor
	 * Increments the total counter of the total number of MathProfessors
	 * 
	 * @param name				{@link MathProfessor#getName()}
	 * @param specialization	{@link MathProfessor#getSpecialization()}
	 */
	public MathProfessor(String name, String specialization) {
		super(name);
		this.specialization = specialization;
		counter++;
	}
	
	/**
	 * Gives a generic math grade to students
	 * 
	 * @return	A better than average grade
	 */
	public static char giveMathGrade() {
		return 'B';
	}

	/**
	 * 
	 * @return		the specialization field of the professor
	 */
	public String getSpecialization() {
		return specialization;
	}
	
	/**
	 * Static Nested Class representing a TA for Math Professors
	 * 
	 * @author jrtobac
	 *
	 */
	public static class MathTA{
		private String taName;
		
		/**
		 * Constructor
		 * 
		 * @param taName	{@link MathTA#getTaName()}
		 */
		public MathTA(String taName){
			this.taName = taName;
		}
		
		/**
		 * 
		 * @return	The ideal number of math professors (which was randomly set at 28)
		 */
		public static int getIdealNumberOfMathProfessors() {
			return 28;
		}
		
		/**
		 * Based on the direction
		 * Gives students a generic grade or a math grade
		 * 
		 * @param giveMathGrade	Boolean value saying to give the student a math grade or a generic grade
		 * 
		 * @return	students grade
		 */
		public static char giveMathTAGrade(boolean giveMathGrade) {
			//getSpecialization();//Cannot make a static reference to the non-static method getSpecialization() from the type MathProfessor
			
			if(giveMathGrade) {
				return giveMathGrade();
			}
			
			return giveGrade();
		}
		
		/**
		 * 
		 * @return	The name of the Math TA
		 */
		public String getTaName() {
			return taName;
		}

		  /*String getProfessorSpecialization() {
			return specialization; //Cannot make a static reference to the non-static field specialization
		}*/
	}
}
