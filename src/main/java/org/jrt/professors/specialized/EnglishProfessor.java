package org.jrt.professors.specialized;

import org.jrt.professors.Professor;

/**
 * Class representing English Professors
 * 
 * @author jrtobac
 *
 */
public class EnglishProfessor extends Professor{
	
	private String poem;
	public static int counter = 0;
	
	/**
	 * static block that gets called at first class reference
	 */
	static {
		System.out.println("In static block of EnglishProfessor");
		//super.name;//Cannot use super in a static context
		//this.poem = "new poem";//Cannot use this in a static context
	}
	
	/**
	 * Constructor
	 * Increments the total number of English Professors 
	 * 
	 * @param name 		{@link EnglishProfessor#getName()}
	 * @param poem		{@link EnglishProfessor#getPoem()}
	 */
	public EnglishProfessor(String name, String poem) {
		super(name);
		this.poem = poem;
		counter++;
	}	
	
	/**
	 * Gives a generic english grade to students
	 * 
	 * @return	A better than average grade
	 */
	public static char giveEnglishGrade() {
		return 'B';
	}
	
	/**
	 * 
	 * @return	The name of the professors favorite poem
	 */
	public String getPoem() {
		return poem;
	}
	
	/**
	 * A static nested class representing the TA for EnglishProfessors
	 * 
	 * @author jrtobac
	 *
	 */
	public static class EnglishTA{
		String taName;
		
		/**
		 * Constructor
		 * 
		 * @param taName	{@link EnglishTA#getTaName()}
		 */
		public EnglishTA(String taName){
			this.taName = taName;
		}
		
		/**
		 * 
		 * @return	The ideal number of English professors (which was randomly set at 32)
		 */
		public static int getIdealNumberOfEnglishProfessors() {
			return 32;
		}
		
		/**
		 * Based on the direction
		 * Gives students a generic grade or a English grade
		 * 
		 * @param giveEnglishGrade	Boolean value saying to give the student an English grade or a generic grade
		 * 
		 * @return	students grade
		 */
		public static char giveEnglishTAGrade(boolean giveEnglishGrade) {			
			if(giveEnglishGrade) {
				return giveEnglishGrade();
			}
			
			return giveGrade();
		}
		
		/**
		 * 
		 * @return	The name of the English TA
		 */
		public String getTaName() {
			return taName;
		}
	}
}
