package org.jrt.professors;
/**
 * Class representing professors
 * 
 * @author jrtobac
 *
 */
public class Professor {
	
	private String name;
	public static int counter = 0;
	
	/**
	 * static block that gets called at first class reference
	 */
	static {
		System.out.println("In static block of Professor");
	}
	
	/**
	 * Constructor
	 * Increments the total number of professors
	 * 
	 * @param name		{@link Professor#getName()}
	 */
	public Professor(String name) {
		this.name = name;
		//this.counter++;//The static field Professor.counter should be accessed in a static way
		counter++;
	}
	
	/**
	 * Gives a generic grade to students
	 * 
	 * @return	An average grade
	 */
	public static char giveGrade() {
		return 'C';
	}
	
	/**
	 * 
	 * @return	the name of the professor
	 */
	public String getName() {
		return name;
	}
}
