package org.jrt.stat;

import org.jrt.professors.Professor;
import org.jrt.professors.specialized.EnglishProfessor;
import org.jrt.professors.specialized.MathProfessor;

/**
 * main class to demonstrate the 'static' keyword in java
 * 
 * @author jrtobac
 *
 */
public class Stat {
	public static void main(String[] args) {
		System.out.println("The number of Professors is: " + Professor.counter);//Professor static block
		System.out.println("Generic Professors give grades as high as: " + Professor.giveGrade());
		Professor prof1 = new Professor("Doug");
		System.out.println("Welcome a new Professor: " + prof1.getName());
		System.out.println("The number of Professors is: " + Professor.counter);
		System.out.println();
		
		System.out.println("Math Professors can give grades as high as: " + MathProfessor.giveMathGrade());//MathProfessor static block
		MathProfessor prof2 = new MathProfessor("Mary", "Calculus");
		System.out.println("Welcome a new Professor: " + prof2.getName() + ", who specializes in " + prof2.getSpecialization());
		MathProfessor prof3 = new MathProfessor("Jennifer", "Algebra");
		System.out.println("Welcome a new Professor: " + prof3.getName() + ", who specializes in " + prof3.getSpecialization());
		System.out.println("The number of Professors is: " + Professor.counter);
		System.out.println("The number of Math Professors is: " + MathProfessor.counter);
		System.out.println();
		
		System.out.println("The ideal number of Math Professors is: " + MathProfessor.MathTA.getIdealNumberOfMathProfessors());
		System.out.println("Math TAs can give a grade as high as: " + MathProfessor.MathTA.giveMathTAGrade(true));
		MathProfessor.MathTA mt = new MathProfessor.MathTA("Sean");
		System.out.println("Welcome a new Math TA " + mt.getTaName());
		System.out.println(mt.getTaName() + " wants to give everyone a " + MathProfessor.MathTA.giveMathTAGrade(false));
		System.out.println("The number of Professors is: " + Professor.counter);
		System.out.println("The number of Math Professors is: " + MathProfessor.counter);
		System.out.println();
		
		System.out.println("English TAs can give a grade as low as: " + EnglishProfessor.EnglishTA.giveEnglishTAGrade(false));
		System.out.println("The ideal number of English Professors is: " + EnglishProfessor.EnglishTA.getIdealNumberOfEnglishProfessors());
		EnglishProfessor.EnglishTA et = new EnglishProfessor.EnglishTA("Mark");
		System.out.println("Welcome a new English TA " + et.getTaName());
		System.out.println(et.getTaName() + " wants to give everyone a " + EnglishProfessor.EnglishTA.giveEnglishTAGrade(true));//EnglishProfessor static block
		System.out.println("The number of Professors is: " + Professor.counter);
		System.out.println("The number of Math Professors is: " + MathProfessor.counter);
		System.out.println("The number of English Professors is: " + EnglishProfessor.counter);
		System.out.println();
		
		System.out.println("English Professors can give grades as high as: " + EnglishProfessor.giveEnglishGrade());
		EnglishProfessor prof4 = new EnglishProfessor("George", "Falling Up");
		System.out.println("Welcome a new Professor: " + prof4.getName() + ", who specializes in " + prof4.getPoem());
		System.out.println("The number of Professors is: " + Professor.counter);
		System.out.println("The number of Math Professors is: " + MathProfessor.counter);
		System.out.println("The number of English Professors is: " + EnglishProfessor.counter);
	}
}
