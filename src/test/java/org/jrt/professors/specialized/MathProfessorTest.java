package org.jrt.professors.specialized;

import static org.junit.jupiter.api.Assertions.*;

import org.jrt.professors.specialized.MathProfessor.MathTA;
import org.junit.jupiter.api.Test;

/**
 * Class for testing the MathProfessor class
 * 
 * @author jrtobac
 *
 */
class MathProfessorTest {
	
	/**
	 * Test that the constructor correctly creates Math professors, and increments the total number of Math professors
	 */
	@Test
	void testMathProfessorConstructor() {
		String name = "test";
		String specialization = "test2";
		int counter = MathProfessor.counter;
		
		MathProfessor mp = new MathProfessor(name, specialization);
		counter++;
		assertEquals(name, mp.getName());
		assertEquals(specialization, mp.getSpecialization());
		assertEquals(counter, MathProfessor.counter);
	}
	
	/**
	 * Test that the professor gives B grades
	 */
	@Test
	void testGiveMathGrade_isB() {
		char expected = 'B';
		assertEquals(expected, MathProfessor.giveMathGrade());
	}
	
	/**
	 * Test that the constructor correctly creates MathTA
	 */
	@Test
	void testMathTAConstructor() {
		String taName = "Test";
		MathProfessor.MathTA mt = new MathProfessor.MathTA(taName);
		assertEquals(taName, mt.getTaName());
	}
	
	/**
	 * Test that the MathTA correctly returns the ideal number of MathProfessors
	 */
	@Test
	void testgetIdealNumberOfMathProfessors_returnsTwentyEight() {
		int idealNum = 28;
		
		assertEquals(idealNum, MathProfessor.MathTA.getIdealNumberOfMathProfessors());
	}
	
	/**
	 * Test that students are given B when input is true
	 * Meaning students should get a math grade
	 */
	@Test
	void testGiveMathTAGrade_giveMathGradeWhenTrue() {
		char expected = 'B';
		char actual = MathTA.giveMathTAGrade(true);
		assertEquals(expected, actual);
	}
	
	/**
	 * Test that students are given C when input is false
	 * Meaning students should get a non-math grade
	 */
	@Test
	void testGiveMathTAGrade_giveNonGradeWhenFalse() {
		char expected = 'C';
		char actual = MathTA.giveMathTAGrade(false);
		assertEquals(expected, actual);
	}

}
