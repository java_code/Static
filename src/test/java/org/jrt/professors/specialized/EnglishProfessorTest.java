package org.jrt.professors.specialized;

import static org.junit.jupiter.api.Assertions.*;

import org.jrt.professors.specialized.EnglishProfessor.EnglishTA;
import org.junit.jupiter.api.Test;

/**
 * Class for testing the EnglishProfessor class
 * 
 * @author jrtobac
 *
 */
class EnglishProfessorTest {
	
	/**
	 * Test that the constructor correctly creates English professors, and increments the total number of English professors
	 */
	@Test
	void testEnglishProfessorConstructor() {
		String name = "test";
		String poem = "test2";
		int counter = EnglishProfessor.counter;
		
		EnglishProfessor mp = new EnglishProfessor(name, poem);
		counter++;
		
		assertEquals(name, mp.getName());
		assertEquals(poem, mp.getPoem());
		assertEquals(counter, EnglishProfessor.counter);
	}
	
	/**
	 * Test that the professor gives B grades
	 */
	@Test
	void testGiveEnglishGrade_isB() {
		char expected = 'B';
		
		assertEquals(expected, EnglishProfessor.giveEnglishGrade());
	}
	
	/**
	 * Test that the constructor correctly creates EnglishTA
	 */
	@Test
	void testEnglishTAConstructor() {
		String taName = "Test";
		EnglishProfessor.EnglishTA mt = new EnglishProfessor.EnglishTA(taName);
		
		assertEquals(taName, mt.getTaName());
	}
	
	/**
	 * Test that the EnglishTA correctly returns the ideal number of EnglishProfessors
	 */
	@Test
	void testgetIdealNumberOfEnglishProfessors_returnsThirtyTwo() {
		int idealNum=32;
		
		assertEquals(idealNum, EnglishProfessor.EnglishTA.getIdealNumberOfEnglishProfessors());
	}
	
	/**
	 * Test that students are given B when input is true
	 * Meaning students should get a English grade
	 */
	@Test
	void testGiveEnglishTAGrade_giveEnglishGradeWhenTrue() {
		char expected = 'B';
		char actual = EnglishTA.giveEnglishTAGrade(true);
		
		assertEquals(expected, actual);
	}
	
	/**
	 * Test that students are given C when input is false
	 * Meaning students should get a non-English grade
	 */
	@Test
	void testGiveEnglishTAGrade_giveNonGradeWhenFalse() {
		char expected = 'C';
		char actual = EnglishTA.giveEnglishTAGrade(false);
		
		assertEquals(expected, actual);
	}

}
