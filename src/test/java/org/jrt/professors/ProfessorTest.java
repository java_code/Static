package org.jrt.professors;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
/**
 * Class to test the Professor class
 * @author jrtobac
 *
 */
class ProfessorTest {

	/**
	 * Test that the constructor correctly creates professors, and increments the total number of professors
	 */
	@Test
	void testProfessorConstructor() {
		String name = "test";
		int counter = Professor.counter;
		
		Professor pf = new Professor(name);
		counter++;
		assertEquals(name, pf.getName());
		assertEquals(counter, Professor.counter);
	}
	
	/**
	 * Test that professors give C grades
	 */
	@Test
	void testGiveGrade_givesCGrade() {
		char expected = 'C';
		assertEquals(expected, Professor.giveGrade());
	}
}
